CREATE SCHEMA IF NOT EXISTS ddos_detector;
CREATE USER IF NOT EXISTS ddos_user IDENTIFIED WITH sha256_password  BY 'myDDoSUser123';
GRANT ALL ON ddos_detector.* TO 'ddos_user';

CREATE TABLE IF NOT EXISTS access_log(
  id BIGINT UNSIGNED AUTO_INCREMENT,
  log_time TIMESTAMP(3) NOT NULL,
  ip VARCHAR(15) NOT NULL,
  request VARCHAR(32),
  status INT,
  header VARCHAR(512),
  PRIMARY KEY (id)
  );
  
CREATE TABLE IF NOT EXISTS block_list(
  id INT UNSIGNED AUTO_INCREMENT,
  ip VARCHAR(15) UNIQUE NOT NULL,
  reason VARCHAR(255),
  block_time TIMESTAMP(3) NOT NULL,
  PRIMARY KEY (id)
  );
