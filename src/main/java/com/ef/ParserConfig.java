package com.ef;

import com.mysql.jdbc.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

public class ParserConfig {

    public static final String DB_PROPERTIES = "db.properties";
    private static final DateTimeFormatter ARGUMENT_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss");
    private static final Logger log = LogManager.getLogger(ParserConfig.class);

    private final String rootPath;
    private Path accessLogPath;
    private LocalDateTime startDate;
    private Duration duration;
    private int threshold;

    public ParserConfig(String[] args) {
        this(args, System.getProperty("user.dir"));
    }

    public ParserConfig(String[] args, String rootPath) {
        this.rootPath = rootPath;
        this.parseArguments(args);
    }

    public Path getAccessLogPath() {
        return accessLogPath;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public Duration getDuration() {
        return duration;
    }

    public int getThreshold() {
        return threshold;
    }

    public Properties getDBProperties() {
        Properties props = new Properties();
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(DB_PROPERTIES)) {
            if (inputStream != null) {
                props.load(inputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }


    /**
     * Parses program arguments and puts them to map
     */
    public void parseArguments(String[] args) {
        Map<String, String> parsedArgs = Arrays.stream(args)
                .map(initialArg -> initialArg.replace("--", "")
                        .toLowerCase()
                        .split("="))
                .filter(splitArg -> splitArg.length == 2)
                .collect(Collectors.toMap(validatedName -> validatedName[0], validatedValue -> validatedValue[1]));

        validateArguments(parsedArgs);
    }

    /**
     * Validates program arguments.
     * Missing arguments are replaced with default values:
     * - access.log - %jar_location%/access.log.
     * - startDate - optional. all 'duration' ranges will be checked.
     * - threshold = 200.
     * - duration - hourly.
     * <p>
     * Minimal validation is done:
     * - threshold should be larger than 0.
     * - access.log must exist.
     * It may be a good idea to make default values configurable, but out of scope of this task.
     * <p>
     * Due to the console nature of the program and due to the technical nature of the task it solves - detection of IP which are suspected in DDoS attack
     * any incorrectly arguments are not replaced by default values.
     * Also any formatting and parsing exceptions are not replaced with user-friendly error messages, because for technical person exceptions are more informative.
     */
    private void validateArguments(Map<String, String> parsedArgs) {
        final String thresholdString = parsedArgs.get("threshold");
        if (StringUtils.isEmptyOrWhitespaceOnly(thresholdString)) {
            this.threshold = 200;
            log.warn("Warning! Zero/non integer/no threshold specified. The default value of {} will be used.", threshold);
        } else {
            this.threshold = Integer.parseInt(thresholdString);
        }

        String durationString = parsedArgs.get("duration");
        if (StringUtils.isEmptyOrWhitespaceOnly(durationString)) {
            duration = Duration.HOURLY;
            log.warn("Warning! No duration specified. The default value of '{}' will be used.", duration);
        } else {
            duration = Duration.valueOf(durationString.toUpperCase());
        }

        String startDateString = parsedArgs.get("startdate");
        if (StringUtils.isEmptyOrWhitespaceOnly(startDateString)) {
            log.warn("Warning! No start date specified. All durations will be checked.");
        } else {
            startDate = LocalDateTime.parse(startDateString, ARGUMENT_FORMATTER);
        }

        String accessLog = parsedArgs.get("accesslog");
        if (StringUtils.isEmptyOrWhitespaceOnly(accessLog)) {
            accessLogPath = Paths.get(rootPath, "access.log");
        } else {
            accessLogPath = Paths.get(accessLog);
        }

        if (!accessLogPath.toFile().exists()) {
            throw new IllegalArgumentException(accessLogPath.toString() + " does not exist");
        }
    }
}
