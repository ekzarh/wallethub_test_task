package com.ef;

import com.mysql.jdbc.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.*;
import java.time.LocalDateTime;

/**
 * Copyright 2017, Denis Lipskis, All rights reserved.
 * This code is an intellectual property of Denis Lipskis who is its sole author.
 * Any WalletHub employee is authorized to read, analyze and run this code to evaluate author's skill as Software Developer.
 * Reading, executing, modification or running this code by any other person or for any other purpose
 * (including but not limited to running against real production data or integration into any other new or existing software)
 * is prohibited and will be persecuted by US law.
 * <p>
 * Min mysql version 5.6.4
 * Min mysql connector(driver) version 5.1.44
 *
 * @author Denis Lipskis
 */
public class Parser {

    public static final String REASON = "Blocked for sending more than %d requests per %s";
    public static final String PERIOD_REASON = " from %s to %s";
    public static final Charset CHARSET = Charset.forName("UTF-8");
    public static final int BUFFER_SIZE = 655350;

    private static final Logger log = LogManager.getLogger(Parser.class);


    private final ConnectionManager connectionManager;
    private final ParserConfig parserConfig;

    private Timestamp lastLoadedTime;


    public Parser(String[] args) {
        this.parserConfig = new ParserConfig(args);
        this.connectionManager = new ConnectionManager(parserConfig);
    }

    public Parser(ParserConfig parserConfig, ConnectionManager connectionManager) {
        this.parserConfig = parserConfig;
        this.connectionManager = connectionManager;
    }

    public static void main(String[] args) {
        new Parser(args).run();
    }

    public void run() {
        try (Connection connection = connectionManager.getConnection()) {
            populateAccessLog(connection);
            if (blockDDoSers(connection)) {
                printBlockedIPs(connection);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * For now I assume that log is well-formed - all entries are present.
     * For even faster population it is possible to create access.log file copy with some tweaks
     * and use MySQL SELECT INFILE to load it directly
     */
    private void populateAccessLog(Connection connection) throws SQLException {
        long start = System.currentTimeMillis();
        int totalInserted = 0;
        initLastLoadedTime(connection);
        connection.setAutoCommit(false); //decreases execution time 100 times
        log.info("File loading started. Last log_time alreaddy loaded {}", lastLoadedTime);
        try (SeekableByteChannel ch = Files.newByteChannel(parserConfig.getAccessLogPath(), StandardOpenOption.READ)) {
            ByteBuffer bb = ByteBuffer.allocateDirect(BUFFER_SIZE);
            String tail = "";
            PreparedStatement insert = connection.prepareStatement(QueryHolder.getInstance().getInsertAccessLogQuery());
            for (; ; ) {
                int n = ch.read(bb);
                bb.flip();
                byte[] bytes = new byte[bb.remaining()];
                bb.get(bytes);
                if (n < 1) {
                    log.info("File loading done.");
                    break;
                }
                String chunk = tail + new String(bytes, CHARSET).replaceAll("\"", "");
                bb.clear();
                String[] rows = chunk.split("\r\n");
                //we may have unfinished log line at the end of the buffer. It has to be kept until next chunk.
                if (n < BUFFER_SIZE || chunk.endsWith("\r\n")) {
                    tail = "";
                } else {
                    tail = rows[(rows.length - 1)];
                }
                final int lastValidRow = StringUtils.isNullOrEmpty(tail) ? rows.length : rows.length - 1;
                int batchRows = 0;
                for (int i = 0; i < lastValidRow; i++) {
                    batchRows += addRow(insert, rows[i]);
                }
                if (batchRows > 0) {
                    insert.executeBatch();
                    connection.commit();
                }
                totalInserted += batchRows;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        connection.setAutoCommit(true);
        log.info("Rows inserted: " + totalInserted);
        log.debug("Time (ms): " + (System.currentTimeMillis() - start));
    }

    /**
     * Inserts all IPs having more than {@link ParserConfig#threshold}  requests per {@link ParserConfig#duration}
     * If {@link ParserConfig#startDate} was specified - checks only specified period. Otherwise checks all periods.
     */
    private boolean blockDDoSers(Connection connection) throws SQLException {
        PreparedStatement preparedStatement;
        if (parserConfig.getStartDate() != null) {
            preparedStatement = connection.prepareStatement(QueryHolder.getInstance().getInsertBlockCandidatesByDateQuery());
            final LocalDateTime startTime = getStartTime();
            final LocalDateTime endTime = getEndTime();
            preparedStatement.setString(1, getReason());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(startTime));
            preparedStatement.setTimestamp(3, Timestamp.valueOf(endTime));
            preparedStatement.setInt(4, parserConfig.getThreshold());

        } else {
            preparedStatement = connection.prepareStatement(QueryHolder.getInstance().getInsertAllBlockCandidatesQuery());
            preparedStatement.setString(1, getReason());
            preparedStatement.setString(2, getSQLRoundingFormat());
            preparedStatement.setInt(3, parserConfig.getThreshold());
        }
        return preparedStatement.executeUpdate() > 0;
    }

    private void printBlockedIPs(Connection connection) throws SQLException {
        ResultSet resultSet = connection.createStatement().executeQuery(QueryHolder.getInstance().getSelectRecentlyBlockedQuery());
        if (resultSet.isBeforeFirst()) {
            log.info("Blocked IP adresses:");
        }
        while (resultSet.next()) {
            log.info("IP:{} Reason: {}", resultSet.getString(1), getReason());
        }
    }

    private void initLastLoadedTime(Connection connection) throws SQLException {
        if (lastLoadedTime == null) {
            final ResultSet resultSet = connection.createStatement().executeQuery(QueryHolder.getInstance().getSelectMaxLogDateQuery());
            lastLoadedTime = new Timestamp(0);
            if (resultSet.next()) {
                final Timestamp timestamp = resultSet.getTimestamp(1);
                if (timestamp != null) {
                    lastLoadedTime = timestamp;
                }
            }
        }
    }

    private int addRow(PreparedStatement insert, String row) throws SQLException {
        String[] columns = row.split("\\|");
        if (columns.length == 5) {
            Timestamp logTime = Timestamp.valueOf(columns[0]);
            if (logTime.after(lastLoadedTime)) {
                insert.setTimestamp(1, logTime);
                for (int i = 1; i < columns.length; i++) {
                    insert.setString(i + 1, columns[i]);
                }
                insert.addBatch();
                return 1;
            }
        } else {
            log.warn("Skipped invalid row: " + row);
        }
        return 0;
    }

    private String getSQLRoundingFormat() {
        return Duration.HOURLY == parserConfig.getDuration() ? "%Y-%m-%d.%H:00:00" : "%Y-%m-%d.00:00:00";
    }

    /**
     * Round down user input in case when minutes and seconds were specified
     */
    private LocalDateTime getEndTime() {
        if (Duration.HOURLY == parserConfig.getDuration()) {
            return getStartTime().plusHours(1);
        } else {
            return getStartTime().plusDays(1);
        }
    }

    private LocalDateTime getStartTime() {
        LocalDateTime result = parserConfig.getStartDate().withMinute(0).withSecond(0);
        if (Duration.DAILY == parserConfig.getDuration()) {
            result = result.withHour(0);
        }
        return result;
    }

    private String getReason() {
        String reason = String.format(REASON, parserConfig.getThreshold(), Duration.HOURLY == parserConfig.getDuration() ? "hour" : "day");
        if (parserConfig.getStartDate() != null) {
            reason += getPeriodReason(getStartTime(), getEndTime());
        }
        return reason;
    }

    private String getPeriodReason(LocalDateTime startDate, LocalDateTime endDate) {
        return String.format(PERIOD_REASON, startDate.toString(), endDate.toString());
    }
}
