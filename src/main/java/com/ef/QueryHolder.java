package com.ef;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStreamReader;

/**
 * Externalizing and naming SQL queries keeps code cleaner and easier to maintain.
 * Also, wrapped in lazy and thread-safe singleton implementation.
 */

//queries are assigned by Gson via reflection, so warning has no meaning
@SuppressWarnings("unused")
public class QueryHolder {
    private static final Gson GSON = new GsonBuilder().create();
    private String insertAccessLogQuery;
    private String selectMaxLogDateQuery;
    private String insertBlockCandidatesByDateQuery;
    private String insertAllBlockCandidatesQuery;
    private String selectRecentlyBlockedQuery;

    private static class QueryHolderLoader {
        private static final QueryHolder INSTANCE = GSON.fromJson(new InputStreamReader(QueryHolder.class.getClassLoader().getResourceAsStream("sql.json")), QueryHolder.class);
    }

    private QueryHolder() {
        if (QueryHolderLoader.INSTANCE != null) {
            throw new IllegalStateException("Already instantiated");
        }
    }

    public static QueryHolder getInstance() {
        return QueryHolderLoader.INSTANCE;
    }

    public String getInsertAccessLogQuery() {
        return insertAccessLogQuery;
    }

    public String getSelectMaxLogDateQuery() {
        return selectMaxLogDateQuery;
    }

    public String getInsertBlockCandidatesByDateQuery() {
        return insertBlockCandidatesByDateQuery;
    }

    public String getInsertAllBlockCandidatesQuery() {
        return insertAllBlockCandidatesQuery;
    }

    public String getSelectRecentlyBlockedQuery() {
        return selectRecentlyBlockedQuery;
    }
}
