package com.ef;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {

    private static final String MYSQL_CONNECTION_URL = "jdbc:mysql://%s:%s/%s?user=%s&password=%s&useSSL=false";
    private final Properties dbProperties;

    public ConnectionManager(ParserConfig parserConfig) {
        this.dbProperties = parserConfig.getDBProperties();
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(String.format(MYSQL_CONNECTION_URL,
                dbProperties.getProperty("host"), dbProperties.getProperty("port"),dbProperties.getProperty("schema"),
                dbProperties.getProperty("user"), dbProperties.getProperty("password")));
    }


}
