package com.ef;

import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@Ignore //configured /src/test/resources/db.properties (with a separate test schema) is required before running integration tests
public class ParserIntegrationTest {

    private Parser parser;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private Connection connection = null;
    private PreparedStatement countAccessLogStatement;
    private PreparedStatement checkBlockListStatement;

    private PreparedStatement cleanupAccessLogsStatement;
    private PreparedStatement cleanupBlockListStatement;

    @Before
    public void setUp() throws Exception {
        File accessLogFile = folder.newFile("access.log");
        Files.copy(getClass().getClassLoader().getResourceAsStream("test_access.log"), Paths.get(accessLogFile.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
        File dbPropsFile = folder.newFile(ParserConfig.DB_PROPERTIES);
        Files.copy(getClass().getClassLoader().getResourceAsStream("db.properties"), Paths.get(dbPropsFile.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);

        ParserConfig parserConfig = new ParserConfig(new String[]{"--startDate=2017-01-01.13:00:00"}, folder.getRoot().getAbsolutePath());
        final ConnectionManager connectionManager = new ConnectionManager(parserConfig);
        connection = connectionManager.getConnection();

        countAccessLogStatement = connection.prepareStatement("SELECT count(*) FROM access_log ORDER BY log_time");
        checkBlockListStatement = connection.prepareStatement("SELECT * FROM block_list ORDER BY ip");
        cleanupAccessLogsStatement = connection.prepareStatement("TRUNCATE access_log");
        cleanupBlockListStatement = connection.prepareStatement("TRUNCATE block_list");
    }

    @Test
    public void loadsAndBlocksHourly() throws Exception {
        ParserConfig parserConfig = new ParserConfig(new String[]{"--startDate=2017-01-01.13:00:00", "--duration=hourly", "--threshold=1"}, folder.getRoot().getAbsolutePath());
        final ConnectionManager connectionManager = new ConnectionManager(parserConfig);
        parser = new Parser(parserConfig, connectionManager);

        parser.run();

        checkAccessLog(10);
        checkBlockList(Arrays.asList("192.168.218.110", "192.168.247.233"));
    }

    @Test
    public void loadsOnlyNewRows() throws Exception {
        ParserConfig parserConfig = new ParserConfig(new String[]{"--startDate=2017-01-01.13:40:01", "--duration=hourly", "--threshold=1"}, folder.getRoot().getAbsolutePath());
        final ConnectionManager connectionManager = new ConnectionManager(parserConfig);
        parser = new Parser(parserConfig, connectionManager);

        parser.run();

        checkAccessLog(10);
        checkBlockList(Arrays.asList("192.168.218.110", "192.168.247.233"));

        File accessLogFile = folder.newFile("access2.log");
        Files.copy(getClass().getClassLoader().getResourceAsStream("test_access2.log"), Paths.get(accessLogFile.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
        parserConfig = new ParserConfig(new String[]{"--accesslog=" + accessLogFile.getAbsolutePath(), "--startDate=2017-01-02.17:00:00", "--duration=hourly", "--threshold=1"},
                folder.getRoot().getAbsolutePath());
        parser = new Parser(parserConfig, connectionManager);

        parser.run();

        checkAccessLog(14);
        checkBlockList(Arrays.asList("192.168.218.110", "192.168.247.233", "192.168.80.199"));
    }

    @Test
    public void loadsAndBlocksDaily() throws Exception {
        ParserConfig parserConfig = new ParserConfig(new String[]{"--startDate=2017-01-01.13:00:00", "--duration=daily", "--threshold=2"}, folder.getRoot().getAbsolutePath());
        final ConnectionManager connectionManager = new ConnectionManager(parserConfig);
        parser = new Parser(parserConfig, connectionManager);

        parser.run();

        checkAccessLog(10);
        checkBlockList(Arrays.asList("192.168.218.110", "192.168.234.82", "192.168.247.233"));
    }


    @Test
    public void loadsAndBlocksWithoutStartDate() throws Exception {
        ParserConfig parserConfig = new ParserConfig(new String[]{"--duration=hourly", "--threshold=1"}, folder.getRoot().getAbsolutePath());
        final ConnectionManager connectionManager = new ConnectionManager(parserConfig);
        parser = new Parser(parserConfig, connectionManager);

        parser.run();

        checkAccessLog(10);
        checkBlockList(Arrays.asList("192.168.218.110", "192.168.234.82", "192.168.247.233"));
    }

    private void checkAccessLog(int count) throws SQLException {
        final ResultSet resultSet = countAccessLogStatement.executeQuery();
        resultSet.next();
        assertEquals(count, resultSet.getInt(1));
    }

    private void checkBlockList(List<String> ips) throws SQLException {
        final ResultSet resultSet = checkBlockListStatement.executeQuery();
        List<String> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(resultSet.getString(2));
        }
        assertArrayEquals(ips.toArray(), result.toArray());
    }

    //test without start date

    private void cleanup(Connection connection) throws SQLException {
        cleanupAccessLogsStatement.executeUpdate();
        cleanupBlockListStatement.executeUpdate();
    }

    @After
    public void tearDown() throws Exception {
        cleanup(connection);
        if (connection != null && !connection.isClosed()) {
            connection.close();
        }
    }
}
