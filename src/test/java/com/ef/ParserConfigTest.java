package com.ef;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.format.DateTimeParseException;
import java.util.Properties;

import static org.junit.Assert.*;

public class ParserConfigTest {

    private ParserConfig parserConfig;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws IOException {
        File file = folder.newFile("access.log");
        Files.copy(getClass().getClassLoader().getResourceAsStream("test_access.log"), Paths.get(file.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
    }

    @Test
    public void testGetDBProperties() throws Exception {
        assertFalse(Paths.get(folder.getRoot().getAbsolutePath(), ParserConfig.DB_PROPERTIES).toFile().exists());
        parserConfig = new ParserConfig(new String[]{""}, folder.getRoot().getAbsolutePath());

        Properties props = parserConfig.getDBProperties();
        assertEquals("localhost", props.getProperty("host"));
        assertEquals("3306", props.getProperty("port"));
        assertEquals("ddos_user", props.getProperty("user"));
    }

    @Test
    public void parseArgumentsDefaultTest() throws Exception {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("access.log does not exist");
        parserConfig = new ParserConfig(new String[]{""});
    }

    @Test
    public void parseArgumentsWithAccessLog() throws Exception {
        parserConfig = new ParserConfig(new String[]{""}, folder.getRoot().getAbsolutePath());
        assertEquals(200, parserConfig.getThreshold());
        assertEquals(Duration.HOURLY, parserConfig.getDuration());
        assertNull(parserConfig.getStartDate());
        assertEquals(Paths.get(folder.getRoot().getAbsolutePath(), "access.log"), parserConfig.getAccessLogPath());
    }

    @Test
    public void parseArgumentsInvalidThreshold() throws Exception {
        exception.expect(NumberFormatException.class);
        exception.expectMessage("For input string: \"a\"");
        parserConfig = new ParserConfig(new String[]{"--threshold=a"}, folder.getRoot().getAbsolutePath());
    }

    @Test
    public void parseArgumentsIgnoresBadArguments() throws Exception {
        parserConfig = new ParserConfig(new String[]{"--threshold="}, folder.getRoot().getAbsolutePath());
        assertEquals(200, parserConfig.getThreshold());
    }

    @Test
    public void parseArgumentsIgnoresBadArguments2() throws Exception {
        parserConfig = new ParserConfig(new String[]{"--threshold=a=b"}, folder.getRoot().getAbsolutePath());
        assertEquals(200, parserConfig.getThreshold());
    }

    @Test
    public void parseArgumentsInvalidDuration() throws Exception {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("No enum constant com.ef.Duration.A");
        parserConfig = new ParserConfig(new String[]{"--duration=a"}, folder.getRoot().getAbsolutePath());
    }

    @Test
    public void parseArgumentsInvalidStartDate() throws Exception {
        exception.expect(DateTimeParseException.class);
        exception.expectMessage("Text 'a' could not be parsed");
        parserConfig = new ParserConfig(new String[]{"--startDate=a"}, folder.getRoot().getAbsolutePath());
    }

    @Test
    public void parseArgumentsInvalidAccessLog() throws Exception {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("a does not exist");
        parserConfig = new ParserConfig(new String[]{"--accesslog=a"}, folder.getRoot().getAbsolutePath());
    }
}