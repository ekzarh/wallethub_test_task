-- Question #1
SELECT ip,
  COUNT(id) cnt
FROM access_log
WHERE log_time >= '2017-01-01.13:00:00'
AND log_time    < '2017-01-01.14:00:00'
GROUP BY ip
HAVING cnt > 100;
--For Oracle (or with  Generated Columns of MySQL >5.7.5 I would create function index (or generated column) and use this alternative
SELECT ip, COUNT(id) cnt
FROM access_log
WHERE DATE_FORMAT(log_time, '%Y-%m-%d.%H:00:00') = '2017-01-01.13:00:00'
GROUP BY ip
HAVING cnt > 100;

-- Question #2
SELECT request 
FROM access_log 
WHERE ip='192.168.11.231';